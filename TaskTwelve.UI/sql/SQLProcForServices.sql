use [OtusContext-20200120102950]
GO

CREATE PROC spStudentsByCourse
	@CourseId int
AS
IF @CourseId IS NOT NULL
	SELECT People.FirstName, People.LastName, Groups.GroupName FROM Courses 
		INNER JOIN Groups ON Courses.Id = Groups.CourseId
		INNER JOIN PersonGroups ON Groups.Id = PersonGroups.Group_Id
		INNER JOIN People ON PersonGroups.Person_Id = People.Id
		WHERE Courses.Id = @CourseId AND People.[Discriminator] = 'Student'
		ORDER BY Groups.Id;
GO

EXEC spStudentsByCourse 1;
GO