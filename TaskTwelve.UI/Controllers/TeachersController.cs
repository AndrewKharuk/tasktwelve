﻿using System.Net;
using System.Web.Mvc;
using TaskTwelve.Core.Models;
using TaskTwelve.UI.Utils;

namespace TaskTwelve.UI.Controllers
{
    public class TeachersController : Controller
    {
        private readonly DataManager _manager;

        public TeachersController()
        {
            _manager = new DataManager();
        }

        public ActionResult Index()
        {
            return View(_manager.Teachers.GetAll());
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Teacher entity = _manager.Teachers.GetById(id.Value);
            if (entity == null)
            {
                return HttpNotFound();
            }
            return View(entity);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Teacher entity)
        {
            if (ModelState.IsValid)
            {
                _manager.Teachers.Add(entity);
                return RedirectToAction("Index");
            }

            return View(entity);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Teacher entity = _manager.Teachers.GetById(id.Value);
            if (entity == null)
            {
                return HttpNotFound();
            }
            return View(entity);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Teacher entity)
        {
            if (ModelState.IsValid)
            {
                _manager.Teachers.Edit(entity);
                return RedirectToAction("Index");
            }
            return View(entity);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Teacher entity = _manager.Teachers.GetById(id.Value);
            if (entity == null)
            {
                return HttpNotFound();
            }
            return View(entity);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            _manager.Teachers.Delete(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _manager.Teachers.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
