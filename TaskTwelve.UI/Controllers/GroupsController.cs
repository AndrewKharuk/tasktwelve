﻿using System.Net;
using System.Web.Mvc;
using TaskTwelve.Core.Models;
using TaskTwelve.UI.Utils;

namespace TaskTwelve.UI.Controllers
{
    public class GroupsController : Controller
    {
        private readonly DataManager _manager;

        public GroupsController()
        {
            _manager = new DataManager();
        }

        public ActionResult Index()
        {
            return View(_manager.Groups.GetAll());
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Group entity = _manager.Groups.GetById(id.Value);
            if (entity == null)
            {
                return HttpNotFound();
            }
            return View(entity);
        }

        public ActionResult Create()
        {
            ViewBag.CourseId = new SelectList(_manager.Courses.GetAll(), "Id", "Name");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Group entity)
        {
            if (ModelState.IsValid)
            {
                _manager.Groups.Add(entity);
                return RedirectToAction("Index");
            }
            ViewBag.CourseId = new SelectList(_manager.Courses.GetAll(), "Id", "Name", entity.CourseId);
            return View(entity);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Group entity = _manager.Groups.GetById(id.Value);
            if (entity == null)
            {
                return HttpNotFound();
            }

            ViewBag.CourseId = new SelectList(_manager.Courses.GetAll(), "Id", "Name", entity.CourseId);
            return View(entity);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Group entity)
        {
            if (ModelState.IsValid)
            {
                _manager.Groups.Edit(entity);
                return RedirectToAction("Index");
            }

            ViewBag.CourseId = new SelectList(_manager.Courses.GetAll(), "Id", "Name", entity.CourseId);
            return View(entity);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Group entity = _manager.Groups.GetById(id.Value);
            if (entity == null)
            {
                return HttpNotFound();
            }
            return View(entity);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            _manager.Groups.Delete(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _manager.Groups.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
