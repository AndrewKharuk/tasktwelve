﻿using System.Net;
using System.Web.Mvc;
using TaskTwelve.Core.Models;
using TaskTwelve.UI.Utils;

namespace TaskTwelve.UI.Controllers
{
    public class CoursesController : Controller
    {
        private readonly DataManager _manager;

        public CoursesController()
        {
            _manager = new DataManager();
        }

        public ActionResult Index()
        {
            return View(_manager.Courses.GetAll());
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Course entity = _manager.Courses.GetById(id.Value);
            if (entity == null)
            {
                return HttpNotFound();
            }
            return View(entity);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Course entity)
        {
            if (ModelState.IsValid)
            {
                _manager.Courses.Add(entity);
                return RedirectToAction("Index");
            }

            return View(entity);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Course entity = _manager.Courses.GetById(id.Value);
            if (entity == null)
            {
                return HttpNotFound();
            }
            return View(entity);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Course entity)
        {
            if (ModelState.IsValid)
            {
                _manager.Courses.Edit(entity);
                return RedirectToAction("Index");
            }
            return View(entity);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Course entity = _manager.Courses.GetById(id.Value);
            if (entity == null)
            {
                return HttpNotFound();
            }
            return View(entity);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            _manager.Courses.Delete(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _manager.Courses.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
