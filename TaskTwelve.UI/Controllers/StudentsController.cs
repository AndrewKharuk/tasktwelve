﻿using System.Net;
using System.Web.Mvc;
using TaskTwelve.Core.Models;
using TaskTwelve.UI.Models;
using TaskTwelve.UI.Utils;

namespace TaskTwelve.UI.Controllers
{
    public class StudentsController : Controller
    {
        private readonly DataManager _manager;

        public StudentsController()
        {
            _manager = new DataManager();
        }

        // GET: Students
        public ActionResult Index()
        {
            return View(_manager.Students.GetAll());
        }

        // GET: Students/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Student student = _manager.Students.GetById(id.Value);
            if (student == null)
            {
                return HttpNotFound();
            }
            return View(student);
        }

        // GET: Students/Create
        public ActionResult Create()
        {
            ViewBag.GroupsId = new MultiSelectList(_manager.Groups.GetAll(), "Id", "GroupName");
            return View();
        }

        // POST: Students/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(StudentViewModel studentViewModel)
        {
            if (ModelState.IsValid)
            {
                Student student = CustomerMapper.MapStudentViewMidelToStudent(studentViewModel);
                _manager.Students.Add(student);
                return RedirectToAction("Index");
            }

            ViewBag.GroupsId = new MultiSelectList(_manager.Groups.GetAll(), "Id", "GroupName", studentViewModel.GroupsId);
            return View(studentViewModel);
        }

        // GET: Students/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Student student = _manager.Students.GetById(id.Value);
            if (student == null)
            {
                return HttpNotFound();
            }

            StudentViewModel studentViewModel = CustomerMapper.MapStudentToStudentViewModel(student);

            ViewBag.GroupsId = new MultiSelectList(_manager.Groups.GetAll(), "Id", "GroupName", studentViewModel.GroupsId);
            return View(studentViewModel);
        }

        // POST: Students/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(StudentViewModel studentViewModel)
        {
            if (ModelState.IsValid)
            {
                Student student = CustomerMapper.MapStudentViewMidelToStudent(studentViewModel);
                _manager.Students.Edit(student);
                return RedirectToAction("Index");
            }

            ViewBag.GroupsId = new MultiSelectList(_manager.Groups.GetAll(), "Id", "GroupName", studentViewModel.GroupsId);
            return View(studentViewModel);
        }

        // GET: Students/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Student student = _manager.Students.GetById(id.Value);
            if (student == null)
            {
                return HttpNotFound();
            }
            return View(student);
        }

        // POST: Students/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            _manager.Students.Delete(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _manager.Students.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
