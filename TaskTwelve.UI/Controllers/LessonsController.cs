﻿using System.Net;
using System.Web.Mvc;
using TaskTwelve.Core.Models;
using TaskTwelve.UI.Utils;

namespace TaskTwelve.UI.Controllers
{
    public class LessonsController : Controller
    {
        private readonly DataManager _manager;

        public LessonsController()
        {
            _manager = new DataManager();
        }

        public ActionResult Index()
        {
            return View(_manager.Lessons.GetAll());
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Lesson entity = _manager.Lessons.GetById(id.Value);
            if (entity == null)
            {
                return HttpNotFound();
            }
            return View(entity);
        }

        public ActionResult Create()
        {
            ViewBag.GroupId = new SelectList(_manager.Groups.GetAll(), "Id", "Name");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Lesson entity)
        {
            if (ModelState.IsValid)
            {
                _manager.Lessons.Add(entity);
                return RedirectToAction("Index");
            }

            ViewBag.GroupId = new SelectList(_manager.Groups.GetAll(), "Id", "Name", entity.GroupId);
            return View(entity);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Lesson entity = _manager.Lessons.GetById(id.Value);
            if (entity == null)
            {
                return HttpNotFound();
            }

            ViewBag.GroupId = new SelectList(_manager.Groups.GetAll(), "Id", "Name", entity.GroupId);
            return View(entity);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Lesson entity)
        {
            if (ModelState.IsValid)
            {
                _manager.Lessons.Edit(entity);
                return RedirectToAction("Index");
            }

            ViewBag.GroupId = new SelectList(_manager.Groups.GetAll(), "Id", "Name", entity.GroupId);
            return View(entity);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Lesson entity = _manager.Lessons.GetById(id.Value);
            if (entity == null)
            {
                return HttpNotFound();
            }
            return View(entity);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            _manager.Lessons.Delete(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _manager.Lessons.Dispose();
                _manager.Groups.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
