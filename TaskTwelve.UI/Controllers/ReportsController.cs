﻿using System.Collections.Generic;
using System.Web.Mvc;
using TaskTwelve.Core.Models;
using TaskTwelve.UI.Utils;

namespace TaskTwelve.UI.Controllers
{
    public class ReportsController : Controller
    {
        private readonly DataManager _manager;

        public ReportsController()
        {
            _manager = new DataManager();
        }

        // GET: Reports
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult StudentsByCourse()
        {
            ViewBag.CourseId = new SelectList(_manager.Courses.GetAll(), "Id", "Name");

            return View();
        }

        public ActionResult StudentsByCourseData(int? courseId)
        {
            IEnumerable<StudentNamesWithGroupReport> data = null;

            if (courseId != null)
                data = _manager.ReportsService.GetStudentsByCourse(courseId.Value);

            return PartialView("_StudentsByCourseData", data);
        }
    }
}