﻿using System;
using System.Collections.Generic;
using TaskTwelve.Core.Models;
using TaskTwelve.UI.Models;

namespace TaskTwelve.UI.Utils
{
    public static class CustomerMapper
    {
        static DataManager _manager = new DataManager();

        public static Student MapStudentViewMidelToStudent(StudentViewModel studentViewModel)
        {
            Student student = new Student();
            ICollection < Group > groups = new List<Group>();

            student.Id = studentViewModel.Id;
            student.FirstName = studentViewModel.FirstName;
            student.LastName = studentViewModel.LastName;
            student.Email = studentViewModel.Email;
            student.City = studentViewModel.City;

            foreach (var item in studentViewModel.GroupsId)
            {
                Group groupById = _manager.Groups.GetById(Int32.Parse(item));
                groups.Add(groupById);
            }

            student.Groups = groups;

            return student;
        }

        public static StudentViewModel MapStudentToStudentViewModel(Student student)
        {
            StudentViewModel studentViewModel = new StudentViewModel();
            ICollection<string> groupsId = new List<string>();

            studentViewModel.Id = student.Id;
            studentViewModel.FirstName = student.FirstName;
            studentViewModel.LastName = student.LastName;
            studentViewModel.Email = student.Email;
            studentViewModel.City = student.City;

            foreach (var item in student.Groups)
            {
                groupsId.Add(item.Id.ToString());
            }

            studentViewModel.GroupsId = groupsId;

            return studentViewModel;
        }
    }
}