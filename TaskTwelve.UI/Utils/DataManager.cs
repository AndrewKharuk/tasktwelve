﻿using System.Configuration;
using TaskTwelve.Core.Interfaces;
using TaskTwelve.Core.Models;
using TaskTwelve.DomainEF.Repositories;
//using TaskTwelve.DomainADO.Repositories;

namespace TaskTwelve.UI.Utils
{
    public class DataManager
    {
        private readonly string _connectionString;

        private IBaseRepository<Course> courses;
        private IBaseRepository<Group> groups; 
        private IBaseRepository<Lesson> lessons;
        private IBaseRepository<Student> students;
        private IBaseRepository<Teacher> teachers;
        private IReportsService reportsService;

        public DataManager()
        {
            _connectionString = ConfigurationManager.ConnectionStrings["OtusContext"].ConnectionString;
        }

        public IBaseRepository<Course> Courses
        {
            get
            {
                if (courses == null)
                    courses = new CoursesRepository(_connectionString);
                return courses;
            }
        }

        public IBaseRepository<Group> Groups
        {
            get
            {
                if (groups == null)
                    groups = new GroupsRepository(_connectionString);
                return groups;
            }
        }

        public IBaseRepository<Lesson> Lessons
        {
            get
            {
                if (lessons == null)
                    lessons = new LessonsRepository(_connectionString);
                return lessons;
            }
        }

        public IBaseRepository<Student> Students
        {
            get
            {
                if (students == null)
                    students = new StudentsRepository(_connectionString);
                return students;
            }
        }

        public IBaseRepository<Teacher> Teachers
        {
            get
            {
                if (teachers == null)
                    teachers = new TeachersRepository(_connectionString);
                return teachers;
            }
        }

        public IReportsService ReportsService
        {
            get
            {
                if (reportsService == null)
                    reportsService = new ReportsService(_connectionString);
                return reportsService;
            }
        }
    }
}