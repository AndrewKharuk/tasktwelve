﻿using System.Collections.Generic;

namespace TaskTwelve.UI.Models
{
    public class StudentViewModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string City { get; set; }

        public ICollection<string> GroupsId { get; set; }

        public StudentViewModel()
        {
            GroupsId = new List<string>();
        }
    }
}