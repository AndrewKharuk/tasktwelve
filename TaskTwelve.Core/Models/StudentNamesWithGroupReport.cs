﻿namespace TaskTwelve.Core.Models
{
    public class StudentNamesWithGroupReport
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string GroupName { get; set; }
    }
}
