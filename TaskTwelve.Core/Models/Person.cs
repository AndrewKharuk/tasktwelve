﻿using System.Collections.Generic;

namespace TaskTwelve.Core.Models
{
    public abstract class Person
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }

        public ICollection<Group> Groups { get; set; }

        public Person()
        {
            Groups = new List<Group>();
        }
    }
}
