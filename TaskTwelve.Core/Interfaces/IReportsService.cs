﻿using System.Collections.Generic;
using TaskTwelve.Core.Models;

namespace TaskTwelve.Core.Interfaces
{
    public interface IReportsService
    {
        IEnumerable<StudentNamesWithGroupReport> GetStudentsByCourse(int id);
    }
}
