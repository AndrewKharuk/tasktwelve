﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using TaskTwelve.Core.Models;

namespace TaskTwelve.DomainEF.EF
{
    public class OtusContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx
    
        public OtusContext(string connectionString) : base(connectionString)
        {
        }

        static OtusContext()
        {
            Database.SetInitializer<OtusContext>(new OtusContextInitializer());
        }

        public System.Data.Entity.DbSet<Course> Courses { get; set; }

        public System.Data.Entity.DbSet<Lesson> Lessons { get; set; }

        public System.Data.Entity.DbSet<Group> Groups { get; set; }

        public System.Data.Entity.DbSet<Teacher> Teachers { get; set; }
        public System.Data.Entity.DbSet<Student> Students { get; set; }
    }
}
