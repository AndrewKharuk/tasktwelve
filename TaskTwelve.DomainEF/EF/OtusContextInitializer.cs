﻿using System.Collections.Generic;
using System.Data.Entity;
using TaskTwelve.Core.Models;

namespace TaskTwelve.DomainEF.EF
{
    public class OtusContextInitializer : DropCreateDatabaseIfModelChanges<OtusContext>     //DropCreateDatabaseIfModelChanges-DropCreateDatabaseAlways
    {
        protected override void Seed(OtusContext context)
        {
            Course course1 = new Course { Name = "CSharp programming" };
            Course course2 = new Course { Name = "Java programming" };
            context.Courses.AddRange(new List<Course> { course1, course2 });

            context.SaveChanges();

            base.Seed(context);
        }
    }
}