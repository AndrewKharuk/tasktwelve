﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskTwelve.Core.Interfaces;
using TaskTwelve.Core.Models;
using TaskTwelve.DomainEF.EF;

namespace TaskTwelve.DomainEF.Repositories
{
    public class ReportsService : IReportsService
    {
        private readonly string _connectionString;

        public ReportsService(string connectionString)
        {
            _connectionString = connectionString;
        }

        public string ConnectionString => _connectionString;

        public IEnumerable<StudentNamesWithGroupReport> GetStudentsByCourse(int id)
        {
            StudentNamesWithGroupReport[] studentsData = null;

            using (OtusContext db = new OtusContext(ConnectionString))
            {
                System.Data.SqlClient.SqlParameter param = new System.Data.SqlClient.SqlParameter("@CourseId", id);

                studentsData = db.Database.SqlQuery<StudentNamesWithGroupReport>("spStudentsByCourse @CourseId", param).ToArray();
            }
           
            return studentsData;
        }
    }
}
