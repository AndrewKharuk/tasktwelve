﻿using TaskTwelve.Core.Models;

namespace TaskTwelve.DomainEF.Repositories
{
    public class TeachersRepository : BaseRepository<Teacher>
    {
        public TeachersRepository(string connectionString) : base(connectionString)
        {
            Table = Context.Teachers;
        }
    }
}
