﻿using TaskTwelve.Core.Models;

namespace TaskTwelve.DomainEF.Repositories
{
    public class LessonsRepository : BaseRepository<Lesson>
    {
        public LessonsRepository(string connectionString) : base(connectionString)
        {
            Table = Context.Lessons;
        }
    }
}
