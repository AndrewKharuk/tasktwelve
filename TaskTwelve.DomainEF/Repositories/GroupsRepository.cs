﻿using TaskTwelve.Core.Models;

namespace TaskTwelve.DomainEF.Repositories
{
    public class GroupsRepository : BaseRepository<Group>
    {
        public GroupsRepository(string connectionString) : base(connectionString)
        {
            Table = Context.Groups;
        }
    }
}
