﻿using TaskTwelve.Core.Models;

namespace TaskTwelve.DomainEF.Repositories
{
    public class CoursesRepository : BaseRepository<Course> 
    {
        public CoursesRepository(string connectionString) : base(connectionString)
        {
            Table = Context.Courses; 
        }
    }
}
