﻿using TaskTwelve.Core.Models;
using System.Data.Entity;
using System.Linq;

namespace TaskTwelve.DomainEF.Repositories
{
    public class StudentsRepository : BaseRepository<Student>
    {
        public StudentsRepository(string connectionString) : base(connectionString)
        {
            Table = Context.Students;
        }

        public override Student GetById(int id)
        {
            return Context.Students.Include(s => s.Groups).FirstOrDefault(s => s.Id == id);
        }
    }
}
