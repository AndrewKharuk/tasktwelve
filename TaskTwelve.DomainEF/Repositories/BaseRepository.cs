﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using TaskTwelve.Core.Interfaces;
using TaskTwelve.DomainEF.EF;

namespace TaskTwelve.DomainEF.Repositories
{
    public abstract class BaseRepository<T> : IBaseRepository<T>
                    where T : class
    {
        private readonly string _connectionString;
        private readonly OtusContext _context;

        public BaseRepository(string connectionString)
        {
            _connectionString = connectionString;
            _context = new OtusContext(_connectionString);

        }

        public OtusContext Context { get { return _context; } }
        public DbSet<T> Table;


        public virtual void Add(T entity)
        {
            Table.Add(entity);
            SaveChanges();
        }

        public virtual void Delete(int id)
        {
            T entity = Table.Find(id);

            if (entity != null)
                Context.Entry(entity).State = EntityState.Deleted;

            Context.SaveChanges();
        }

        public virtual void Edit(T entity)
        {
            Context.Entry(entity).State = EntityState.Modified;

            Context.SaveChanges();
        }

        public virtual IEnumerable<T> GetAll()
        {
            return Table.ToList<T>();
        }

        public virtual T GetById(int id)
        {
            return Table.Find(id);
        }

        internal void SaveChanges()
        {
            try
            {
                Context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        #region IDisposable Support
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    Context.Dispose();
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
