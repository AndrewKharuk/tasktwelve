﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskTwelve.Core.Interfaces;
using TaskTwelve.Core.Models;

namespace TaskTwelve.DomainADO.Repositories
{
    public class ReportsService : IReportsService
    {
        private readonly string _connectionString;

        public ReportsService(string connectionString)
        {
            _connectionString = connectionString;
        }

        public string ConnectionString => _connectionString;

        public IEnumerable<StudentNamesWithGroupReport> GetStudentsByCourse(int id)
        {
            string sqlExpression = "spStudentsByCourse";

            ICollection<StudentNamesWithGroupReport> studentsList = new List<StudentNamesWithGroupReport>();

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                SqlCommand command = new SqlCommand(sqlExpression, connection);

                command.CommandType = System.Data.CommandType.StoredProcedure;

                SqlParameter nameParam = new SqlParameter
                {
                    ParameterName = "@CourseId",
                    Value = id
                };
            
                command.Parameters.Add(nameParam);

                var reader = command.ExecuteReader();

                while (reader.Read())
                {
                    studentsList.Add(new StudentNamesWithGroupReport
                    {
                        FirstName = reader.GetString(0),
                        LastName = reader.GetString(1),
                        GroupName = reader.GetString(2)
                    });
                }

                reader.Close();                  
            }

            return studentsList;
        }
    }
}
