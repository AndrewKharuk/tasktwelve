﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskTwelve.Core.Models;

namespace TaskTwelve.DomainADO.Repositories
{
    public class TeachersRepository : BaseRepository<Teacher>
    {
        public TeachersRepository(string connectionString) : base(connectionString)
        {
        }

        public override void Add(Teacher entity)
        {
            string sql = "Insert Into People" +
                "(FirstName, LastName, Email, Phone, Discriminator) " +
                $"Values ('{entity.FirstName}', '{entity.LastName}', '{entity.Email}', '{entity.Phone}', 'Teacher')";

            ExecuteSql(sql);
        }

        public override void Delete(int id)
        {
            string sql = $"Delete from People where Id = '{id}'";

            ExecuteSql(sql);
        }

        public override void Edit(Teacher entity)
        {
            string sql = "Update People Set " +
                $"FirstName = '{entity.FirstName}', LastName = '{entity.LastName}', Email = '{entity.Email}', Phone = '{entity.Phone}' where Id = '{entity.Id}'";

            ExecuteSql(sql);
        }

        public override IEnumerable<Teacher> GetAll()
        {
            string sql = "Select * from People where Discriminator = 'Teacher'";
            ICollection<Teacher> courses = new List<Teacher>();

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(sql, connection);

                using (SqlDataReader myDataReader = command.ExecuteReader())
                {
                    while (myDataReader.Read())
                    {
                        courses.Add(new Teacher
                        {
                            Id = (int)myDataReader["Id"],
                            FirstName = (string)myDataReader["FirstName"],
                            LastName = (string)myDataReader["LastName"],
                            Email = (string)myDataReader["Email"],
                            Phone = (string)myDataReader["Phone"]
                        });
                    }
                }
            }
            return courses;
        }

        public override Teacher GetById(int id)
        {
            string sql = $"Select * from People where Id = '{id}'";

            Teacher entity = new Teacher();

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(sql, connection);

                using (SqlDataReader myDataReader = command.ExecuteReader())
                {
                    while (myDataReader.Read())
                    {
                        entity.Id = (int)myDataReader["Id"];
                        entity.FirstName = (string)myDataReader["FirstName"];
                        entity.LastName = (string)myDataReader["LastName"];
                        entity.Email = (string)myDataReader["Email"];
                        entity.Phone = (string)myDataReader["Phone"];
                    }
                }
            }
            return entity;
        }
    }
}
