﻿using System.Collections.Generic;
using System.Data.SqlClient;
using TaskTwelve.Core.Models;

namespace TaskTwelve.DomainADO.Repositories
{
    public class CoursesRepository : BaseRepository<Course>
    {
        public CoursesRepository(string connectionString) : base(connectionString)
        {
        }
        
        public override void Add(Course entity)
        {
            string sql = "Insert Into Courses" +
                $"(Name) Values ('{entity.Name}')";

            ExecuteSql(sql);
        }

        public override void Delete(int id)
        {
            string sql = $"Delete from Courses where Id = '{id}'";

            ExecuteSql(sql);
        }
        
        public override void Edit(Course entity)
        {
            string sql = "Update Courses Set " +
                $"Name = '{entity.Name}' where Id = '{entity.Id}'";

            ExecuteSql(sql);
        }

        public override IEnumerable<Course> GetAll()
        {
            string sql = "Select * from Courses";
            ICollection<Course> courses = new List<Course>();

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(sql, connection);

                using (SqlDataReader myDataReader = command.ExecuteReader())
                {
                    while (myDataReader.Read())
                    {
                        courses.Add(new Course
                        {
                            Id = (int)myDataReader["Id"],
                            Name = (string)myDataReader["Name"]
                        });
                    }
                }
            }
            return courses;
        }

        public override Course GetById(int id)
        {
            string sql = $"Select * from Courses where Id = '{id}'";

            Course entity = new Course();

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(sql, connection);

                using (SqlDataReader myDataReader = command.ExecuteReader())
                {
                    while (myDataReader.Read())
                    {
                        entity.Id = (int)myDataReader["Id"];
                        entity.Name = (string)myDataReader["Name"];
                    }  
                }
            }
            return entity;
        }
    }
}
