﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskTwelve.Core.Models;

namespace TaskTwelve.DomainADO.Repositories
{
    public class LessonsRepository : BaseRepository<Lesson>
    {
        public LessonsRepository(string connectionString) : base(connectionString)
        {
        }

        public override void Add(Lesson entity)
        {
            string sql = "Insert Into Lessons" +
                $"(Name, Date, GroupId) Values ('{entity.Name}', '{entity.Date}', '{entity.GroupId}')" ;

            ExecuteSql(sql);
        }

        public override void Delete(int id)
        {
            string sql = $"Delete from Lessons where Id = '{id}'";

            ExecuteSql(sql);
        }

        public override void Edit(Lesson entity)
        {
            string sql = "Update Lessons Set " +
                $"Name = '{entity.Name}', Date = '{entity.Date}', GroupId = '{entity.GroupId}' where Id = '{entity.Id}'";

            ExecuteSql(sql);
        }

        public override IEnumerable<Lesson> GetAll()
        {
            string sql = "Select * from Lessons";
            ICollection<Lesson> courses = new List<Lesson>();

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(sql, connection);

                using (SqlDataReader myDataReader = command.ExecuteReader())
                {
                    while (myDataReader.Read())
                    {
                        courses.Add(new Lesson
                        {
                            Id = (int)myDataReader["Id"],
                            Name = (string)myDataReader["Name"],
                            Date = (DateTime)myDataReader["Date"],
                            GroupId = (int)myDataReader["GroupId"]
                        });
                    }
                }
            }
            return courses;
        }

        public override Lesson GetById(int id)
        {
            string sql = $"Select * from Lessons where Id = '{id}'";

            Lesson entity = new Lesson();

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(sql, connection);

                using (SqlDataReader myDataReader = command.ExecuteReader())
                {
                    while (myDataReader.Read())
                    {
                        entity.Id = (int)myDataReader["Id"];
                        entity.Name = (string)myDataReader["Name"];
                        entity.Date = (DateTime)myDataReader["Date"];
                        entity.GroupId = (int)myDataReader["GroupId"];
                    }
                }
            }
            return entity;
        }
    }
}
