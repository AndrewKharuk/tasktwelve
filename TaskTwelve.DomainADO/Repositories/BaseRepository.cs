﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using TaskTwelve.Core.Interfaces;

namespace TaskTwelve.DomainADO.Repositories
{
    public abstract class BaseRepository<T> : IBaseRepository<T> where T : class, new()
    {
        private readonly string _connectionString;

        public BaseRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public string ConnectionString => _connectionString;

        public virtual void Add(T entity)
        {
        }

        public virtual void Delete(int id)
        {
        }

        public virtual void Edit(T entity)
        {
        }

        public virtual IEnumerable<T> GetAll()
        {
            return new List<T>();
        }

        public virtual T GetById(int id)
        {
            return new T();
        }

        protected void ExecuteSql(string sql)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(sql, connection);
                command.ExecuteNonQuery();
            }
        }

        protected void ExecuteSqlTransaction(string sqlOne = null, string sqlTwo = null, string sqlThree = null)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();

                SqlCommand command = connection.CreateCommand();
                command.Transaction = transaction;

                try
                {
                    if (sqlOne != null)
                    {
                        command.CommandText = sqlOne;
                        command.ExecuteNonQuery();
                    }

                    if (sqlTwo != null)
                    {
                        command.CommandText = sqlTwo;
                        command.ExecuteNonQuery();
                    }

                    if (sqlThree != null)
                    {
                        command.CommandText = sqlThree;
                        command.ExecuteNonQuery();
                    }

                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw;
                }
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // Для определения избыточных вызовов

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: освободить управляемое состояние (управляемые объекты).
                }

                // TODO: освободить неуправляемые ресурсы (неуправляемые объекты) и переопределить ниже метод завершения.
                // TODO: задать большим полям значение NULL.

                disposedValue = true;
            }
        }

        // TODO: переопределить метод завершения, только если Dispose(bool disposing) выше включает код для освобождения неуправляемых ресурсов.
        // ~BaseRepository() {
        //   // Не изменяйте этот код. Разместите код очистки выше, в методе Dispose(bool disposing).
        //   Dispose(false);
        // }

        // Этот код добавлен для правильной реализации шаблона высвобождаемого класса.
        public void Dispose()
        {
            // Не изменяйте этот код. Разместите код очистки выше, в методе Dispose(bool disposing).
            Dispose(true);
            // TODO: раскомментировать следующую строку, если метод завершения переопределен выше.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
