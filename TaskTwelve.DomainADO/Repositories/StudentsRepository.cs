﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using TaskTwelve.Core.Models;

namespace TaskTwelve.DomainADO.Repositories
{
    public class StudentsRepository : BaseRepository<Student>
    {
        public StudentsRepository(string connectionString) : base(connectionString)
        {
        }

        public override void Add(Student entity)
        {
            string sqlPeople = "Insert Into People" +
                "(FirstName, LastName, Email, City, Discriminator) " +
                $"Values ('{entity.FirstName}', '{entity.LastName}', '{entity.Email}', '{entity.City}', 'Student'); "
                + "SELECT CAST(scope_identity() AS int)";

            int peopleId = 0;

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();

                SqlTransaction transaction = connection.BeginTransaction();

                SqlCommand command = connection.CreateCommand();
                command.Transaction = transaction;

                try
                {
                    command.CommandText = sqlPeople;
                    peopleId = (int)command.ExecuteScalar();

                    string sqlPersonGroups = "Insert Into PersonGroups (Person_Id, Group_Id) Values ";
                    foreach (var item in entity.Groups)
                    {
                        sqlPersonGroups += $"('{peopleId}', '{item.Id}'),";
                    }

                    command.CommandText = sqlPersonGroups.TrimEnd(',');
                    command.ExecuteNonQuery();

                    transaction.Commit();
                }
                catch (System.Exception)
                {
                    transaction.Rollback();
                    throw;
                }
            }
        }

        public override void Delete(int id)
        {
            string sqlPersonGroups = $"Delete from PersonGroups where Person_Id = '{id}'";
            string sqlPeople = $"Delete from People where Id = '{id}'";

            ExecuteSqlTransaction(sqlPersonGroups, sqlPeople);
        }

        public override void Edit(Student entity)
        {
            string sqlPeople = "Update People Set " +
                $"FirstName = '{entity.FirstName}', LastName = '{entity.LastName}', Email = '{entity.Email}', City = '{entity.City}' where Id = '{entity.Id}'";

            string sqlPersonGroupsDelete = $"Delete from PersonGroups where Person_Id = '{entity.Id}'";

            string sqlPersonGroupsInsert = "Insert Into PersonGroups (Person_Id, Group_Id) Values ";

            foreach (var item in entity.Groups)
            {
                sqlPersonGroupsInsert += $"('{entity.Id}', '{item.Id}'),";
            }
            var a = sqlPersonGroupsInsert.TrimEnd(',');


            ExecuteSqlTransaction(sqlPeople, sqlPersonGroupsDelete, sqlPersonGroupsInsert.TrimEnd(','));
        }

        public override IEnumerable<Student> GetAll()
        {
            string sql = "Select * from People where Discriminator = 'Student'";
            ICollection<Student> courses = new List<Student>();

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(sql, connection);

                using (SqlDataReader myDataReader = command.ExecuteReader())
                {
                    while (myDataReader.Read())
                    {
                        courses.Add(new Student
                        {
                            Id = (int)myDataReader["Id"],
                            FirstName = (string)myDataReader["FirstName"],
                            LastName = (string)myDataReader["LastName"],
                            Email = (string)myDataReader["Email"],
                            City = (string)myDataReader["City"]
                        });
                    }
                }
            }
            return courses;
        }

        public override Student GetById(int id)
        {
            string sql = $"Select * from People where Id = '{id}'";
            string sqlforGroups = $"Select Groups.Id, Groups.GroupName, Groups.StartDate, Groups.EndDate, Groups.CourseId from PersonGroups " 
                + $"Inner Join Groups On PersonGroups.Group_Id = Groups.Id where PersonGroups.Person_Id = '{id}'";

            Student entity = new Student();
            ICollection<Group> groups = new List<Group>();

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                command.CommandText = sql;

                using (SqlDataReader myDataReader = command.ExecuteReader())
                {
                    while (myDataReader.Read())
                    {
                        entity.Id = (int)myDataReader["Id"];
                        entity.FirstName = (string)myDataReader["FirstName"];
                        entity.LastName = (string)myDataReader["LastName"];
                        entity.Email = (string)myDataReader["Email"];
                        entity.City = (string)myDataReader["City"];
                    }
                }

                command.CommandText = sqlforGroups;
                using (SqlDataReader myDataReader = command.ExecuteReader())
                {
                    while (myDataReader.Read())
                    {
                        groups.Add(new Group {
                            Id = (int)myDataReader["Id"],
                            GroupName = (string)myDataReader["GroupName"],
                                    StartDate = (DateTime)myDataReader["StartDate"],
                                    EndDate = (DateTime)myDataReader["EndDate"],
                            CourseId = (int)myDataReader["CourseId"]
                        });
                    }
                }

                entity.Groups = groups;
            }
            return entity;
        }
    }
}
