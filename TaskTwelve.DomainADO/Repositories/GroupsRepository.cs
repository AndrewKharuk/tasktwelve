﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using TaskTwelve.Core.Models;

namespace TaskTwelve.DomainADO.Repositories
{
    public class GroupsRepository : BaseRepository<Group>
    {
        public GroupsRepository(string connectionString) : base(connectionString)
        {
        }

        public override void Add(Group entity)
        {
            string sql = "Insert Into Groups" +
                $"(GroupName, StartDate, EndDate, CourseId) Values ('{entity.GroupName}', '{entity.StartDate}', '{entity.EndDate}', '{entity.CourseId}')";

            ExecuteSql(sql);
        }

        public override void Delete(int id)
        {
            string sql = $"Delete from Groups where Id = '{id}'";

            ExecuteSql(sql);
        }

        public override void Edit(Group entity)
        {
            string sql = "Update Groups Set " +
                $"GroupName = '{entity.GroupName}', StartDate = '{entity.StartDate}', EndDate = '{entity.EndDate}', CourseId = '{entity.CourseId}' where Id = '{entity.Id}'";

            ExecuteSql(sql);
        }

        public override IEnumerable<Group> GetAll()
        {
            string sql = "Select * from Groups";
            ICollection<Group> list = new List<Group>();

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(sql, connection);

                using (SqlDataReader myDataReader = command.ExecuteReader())
                {
                    while (myDataReader.Read())
                    {
                        list.Add(new Group
                        {
                            Id = (int)myDataReader["Id"],
                            GroupName = (string)myDataReader["GroupName"],
                            StartDate = (DateTime)myDataReader["StartDate"],
                            EndDate= (DateTime)myDataReader["EndDate"],
                            CourseId = (int)myDataReader["CourseId"]
                        });
                    }
                }
            }
            return list;
        }

        public override Group GetById(int id)
        {
            string sql = $"Select * from Groups where Id = '{id}'";

            Group entity = new Group();

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(sql, connection);

                using (SqlDataReader myDataReader = command.ExecuteReader())
                {
                    while (myDataReader.Read())
                    {
                        entity.Id = (int)myDataReader["Id"];
                        entity.GroupName = (string)myDataReader["GroupName"];
                        entity.StartDate = (DateTime)myDataReader["StartDate"];
                        entity.EndDate = (DateTime)myDataReader["EndDate"];
                        entity.CourseId = (int)myDataReader["CourseId"];
                    }
                }
            }
            return entity;
        }
    }
}
